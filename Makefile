help:
	@echo 'clean'
	@echo 'eject'
	@echo 'psx'

unmount: eject

eject: clean
	diskutil unmount force P

clean:
	bin/clean.sh

psx:
	yarn psx

PHONY: clean unmount
