# Instructions

* run `make eject` to clean up mac files and unmount disk
* run `make psx` to generate psx playlist files and clean up readme.html files

## apps
* retroarch bundle
    * http://retroarch.com/?page=platforms
* sys-ftpd
    * https://sdsetup.com/console?switch
* nx shell
    * https://github.com/joel16/NX-Shell/releases
* pplay
    * https://github.com/Cpasjuste/pplay/releases
* nx mtp
    * https://github.com/liuervehc/nxmtp/releases
    * update `atmosphere/system_settings.ini`
        * change `usb30_force_enabled` to `u8!0x0`

## cfw/payload
* atmosphere
    * https://github.com/Atmosphere-NX/Atmosphere/releases
    * https://switchbrew.org/wiki/Title_list/Games
        * 01004B7009F00000 (miles and kilo)
* hbloader
    * https://github.com/switchbrew/nx-hbloader/releases
* hbmenu
    * https://github.com/switchbrew/nx-hbmenu/releases
* hekate
    * https://github.com/CTCaer/hekate/releases
* rekado.apk
    * https://github.com/MenosGrante/Rekado/releases
