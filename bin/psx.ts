import { readdirSync, existsSync, statSync, unlinkSync, writeFileSync } from 'fs';

const SD_ROOT = '/Volumes/P';
// const SD_ROOT = '.';
const ROM_ROOT = '/external/roms/psx';
const PLAYLIST_FILE = '/external/playlists/Sony - PlayStation.lpl';

if (!existsSync(`${SD_ROOT}${ROM_ROOT}`)) {
    console.log('psx rom dir does not exist');
    process.exit(1);
}

const output = { version: "1.0", items: [] };
const cueFiles = traverseDir(ROM_ROOT, []);

for (const file of cueFiles) {
    output.items.push(generatePlaylistItem(file));
}

if (existsSync(`${SD_ROOT}${PLAYLIST_FILE}`)) {
    console.log('deleting old playlist file');
    unlinkSync(`${SD_ROOT}${PLAYLIST_FILE}`);
}
writeFileSync(`${SD_ROOT}${PLAYLIST_FILE}`, JSON.stringify(output, undefined, 2));

function generatePlaylistItem(path: string) {
    return {
        path,
        label: path.substring(path.lastIndexOf('/')+1, path.length - 4),
        core_path: "DETECT",
        core_name: "DETECT",
        crc32: "DETECT",
        db_name: "Sony - PlayStation.lpl",
    };
}

function traverseDir(fullPath: string, cueFiles: string[]): string[] {
    const files: string[] = readdirSync(`${SD_ROOT}${fullPath}`);
    
    for (const file of files) {
        cueFiles = processFile(fullPath, file, cueFiles);
    }

    return cueFiles;
}

function processFile(prefix: string, curFile: string, cueFiles: string[]): string[] {
    const fullPath = `${prefix}/${curFile}`;

    if (!existsSync(`${SD_ROOT}${fullPath}`)) {
        console.log(`${fullPath} not exist`);
        return cueFiles;
    }

    const stats = statSync(`${SD_ROOT}${fullPath}`);

    if (stats.isDirectory()) {
        return traverseDir(fullPath, cueFiles);
    }

    if (curFile === 'readme.html') {
        console.log(`deleting ${fullPath}`);
        unlinkSync(`${SD_ROOT}${fullPath}`);
        return cueFiles;
    }

    if (curFile.substr(curFile.length-4, 4) === '.cue') {
        cueFiles.push(fullPath);
    }

    return cueFiles;
}
