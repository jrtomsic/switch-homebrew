#!/usr/bin/env php
<?php
const SDROOT_DIR = '/Volumes/P';
// const SDROOT_DIR = __DIR__ . '/..';
const ROM_DIR = 'external/roms';
const CORE_DIR = 'retroarch/cores/switch';
const PLAYLIST_DIR = 'external/playlists';

main();

function main()
{
    $playlistFiles = [
        'gba' => 'gba-next.nro',
        'gbc' => 'gambatte.nro',
        'genesis' => 'genesis_plus_gx.nro',
        'nes' => 'fceumm.nro',
        'snes' => 'snes9x.nro',
    ];

    foreach ($playlistFiles as $roms => $core) {
        $playlistFullPath = sprintf('%s/%s/%s.lpl', SDROOT_DIR, PLAYLIST_DIR, $roms);

        $romFiles = generateFileArray(
            sprintf('%s/%s', SDROOT_DIR, ROM_DIR),
            $roms
        );

        $playlistContents = generateLPLContent(
            sprintf('%s.lpl', $roms),
            $core,
            $romFiles
        );

        file_put_contents($playlistFullPath, $playlistContents);
    }
}

function generateRomName(string $romFile)
{
    $matches = [];
    preg_match('/([^\/]+)\..{2,4}$/', $romFile, $matches);
    return $matches[1];
}

// var_export(generateRomName(
//     sprintf('\%s\%s\%s', ROOT_DIR, ROM_DIR, 'gba/blah thing.gba')
// ));

function generateLPLContent(
    string $lplFile,
    string $coreFile,
    array $romFiles
) {
    $contents = '';

    foreach ($romFiles as $romFile) {
        $contents .= sprintf(
            "%s\n%s\n%s\n%s\n%s\n%s\n",
            sprintf('/%s/%s', ROM_DIR, $romFile),
            generateRomName($romFile),
            sprintf('/%s/%s', CORE_DIR, $coreFile),
            'DETECT',
            'DETECT',
            $lplFile
        );
    }

    return $contents;
}

// var_export(generateLPLContent(
//     'blah.lpl',
//     'blah.nro',
//     [
//         'gba/one.zip',
//         'gba/two.gba',
//         'gba/three.7z',
//         'gba/four.asdf',
//         'gba/five.gb',
//     ]
// ));

function generateFileArray(string $pathToRoms, string $pathFromRoms)
{
    /** @var string[] $files */
    $files = [];

    /** @var SplFileInfo[] $iterator */
    $iterator = new FilesystemIterator(
        sprintf('%s/%s', $pathToRoms, $pathFromRoms)
    );

    foreach ($iterator as $file) {
        if ($file->isDir()) {
            $files = array_merge(
                $files,
                generateFileArray(
                    $pathToRoms,
                    sprintf('%s/%s', $pathFromRoms, $file->getFilename())
                )
            );

            continue;
        }

        if (preg_match('/.zip$/', $file->getFilename()) === 1) {
            $files[] = sprintf('%s/%s', $pathFromRoms, $file->getFilename());
        }
    }

    return $files;
}

// var_export(generateFileArray(
//     sprintf('%s/%s', SDROOT_DIR, ROM_DIR),
//     'gba'
// ));