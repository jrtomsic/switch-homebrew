#!/bin/sh

diskutil list P
if [ $? != 0 ]; then
    echo 'aborting: failed to find P drive'
    exit 1
fi

cd /Volumes/P
if [ $? != 0 ]; then
    echo 'aborting: failed to change dirs'
    exit 1
fi

dot_clean .
find . -iname '.DS_Store' -exec rm -rfv "{}" \;
find . -type d -iname '.Trashes' -exec rm -rfv "{}" \;

find . -type d -not -name Nintendo -depth 1 -exec sudo chflags -Rv arch "{}" \;
find . -type f -not -name Nintendo -depth 1 -exec sudo chflags -v arch "{}" \;

find . -type d -iname '.Spotlight-V100' -exec rm -rfv "{}" \;
find . -iname '.fseventsd' -exec rm -rfv "{}" \;
find . -type d -iname '.Trashes' -exec rm -rfv "{}" \;

cd -
